import datetime

from flask import session


def add_to_basket(item: dict) -> None:
    basket = session.get('basket', [])
    for it in basket:
        if it['c_id'] == item['c_id']:
            return
    item['birthday'] = str(item['birthday'])
    basket.append(item)
    session['basket'] = basket


def clear_basket() -> None:
    if 'basket' in session:
        session.pop('basket')

from functools import wraps
from flask import session, request, current_app, render_template


def get_role():
    group_name = session.get('group_name', 'unauthorized')
    if group_name == 'unauthorized':
        return 'гость'
    elif group_name == 'hr':
        return 'HR'
    elif group_name == 'chef':
        return 'начальник'
    elif group_name == 'admin':
        return 'администратор'


def group_permission_validation(app: str = None):
    access_config = current_app.config['ACCESS_CONFIG']
    group_name = session.get('group_name', 'unauthorized')

    if app is None:
        endpoint = request.endpoint
    else:
        endpoint = app

    if len(endpoint.split('.')) == 1:
        target_app = ""
    else:
        target_app = endpoint

    if group_name in access_config and target_app in access_config[group_name]:
        return True
    return False


def group_permission_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if group_permission_validation():
            return f(*args, **kwargs)
        return render_template('forbidden.html')

    return wrapper
